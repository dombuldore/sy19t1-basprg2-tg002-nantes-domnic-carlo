#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

/*In printWinner function
the DRAW (Civ = Civ) mechanic is not working. I couldn't get it to work for one or more reason(s).
Game Proceeds to another round if BOTH player and ai uses CIVILIAN cards.

The Rest of the Program works. Up till Round 12. */

void fillEmperor(vector<string>& empVector);
void fillSlave(vector<string>& slaVector);
void printDeck(vector<string>& playerCardVector, int playerDeckSize);
void distributeDeck(vector<string>& playerCardVector, vector<string>& aiCardVector, int matchRound);
void playerCardPick(const vector<string>& playerCards, int plDeckSize, int userPick);
void computerCardPick(const vector<string>& aiCards, int aiDeckSize, int aiPick);
void printVersus(const vector<string>&, const vector<string>&, int, int);
int printWinner(vector<string>& playerCards, vector<string>& aiCards, int userPick, int aiPick, int* currentGold, int* earDistance, int earBet);

int main()
{
	srand(time(NULL));
	//Variable Declarations
	vector<string> playerCards;
	vector<string> aiCards;
	int goldCoins = 0;
	int earDistance = 30;
	int earBet;

	int playerDeckSize = 5;
	int aiDeckSize = 5;

	int plCardPick;
	int aiCardPick = rand() % 5 + 1;

	char decision;
	

	//Game Round LOOP
	int matchRound = 1;
	while (matchRound != 13 && earDistance != 0)
	{
		//Ear Bet
		cout << "=========== ROUND " << matchRound << " out of 12 ===========" << endl;
		cout << "Current Gold :: " << goldCoins << endl;
		cout << "Current distance from Death in Millimeters (mm) :: " << earDistance << endl;
		cout << "How many Millimeters (mm) do you want to bet, Champion? :: ";
		cin >> earBet;
		system("CLS");

		//Flavor Stats
		cout << "You have wagered " << earBet << " Millimeters (mm)" << endl << endl << endl;
		cout << "Current Gold :: " << goldCoins << endl;
		cout << "You still have " << earDistance << " Millimeters (mm)." << endl;
		cout << "=========== ROUND " << matchRound << " out of 12 ===========" << endl;

		if (matchRound <= 3 || (matchRound >= 7 && matchRound <= 9))
		{
			cout << "Champion, you will be playing as the EMPEROR." << endl;
			fillEmperor(playerCards);
			fillSlave(aiCards);
		}
		else
		{
			cout << "Champion, you will be playing as the SLAVE." << endl;
			fillSlave(playerCards);
			fillEmperor(aiCards);
		}

		//In Case of DRAW
		/*do (NOT WORKING)
		{*/
			//Player + AI Chooses card
			printDeck(playerCards, playerDeckSize);
			cout << "Choose your Card :: ";
			cin >> plCardPick;
			aiCardPick;
			playerCardPick(playerCards, playerDeckSize, plCardPick);

			cout << "Press Y to show your card to Arthur :: ";
			cin >> decision;

			cout << endl << endl;
			//Display AI Chosen Card
			cout << "Arthur Ino is choosing..." << endl;
			computerCardPick(aiCards, aiDeckSize, aiCardPick);

			cout << "Proceed [Y] :: ";
			cin >> decision;
			system("CLS");

			//Decision Round
			printVersus(playerCards, aiCards, plCardPick, aiCardPick);
			printWinner(playerCards, aiCards, plCardPick, aiCardPick, &goldCoins, &earDistance, earBet);

		//} while (printWinner(playerCards, aiCards, plCardPick, aiCardPick, &goldCoins, &earDistance, earBet) != 1 && playerDeckSize <= 1);

		cout << "Proceed [Y] :: ";
		cin >> decision;

		//Delete Cards from Deck if DRAW
		//End of Round Vector Clear
		playerCards.clear();
		aiCards.clear();
		matchRound++;
		system("CLS");
	}

	if (matchRound == 12)
	{
		//Best Ending
		if (goldCoins >= 20000000 && earDistance <= 1)
		{
			cout << "CONGRATULATIONS! You have achieved the BEST ENDING." << endl << "Thank you for playing." << endl;
		}
		else if (goldCoins < 20000000 && earDistance <= 1)
		{
			cout << "Grats. You survived... but you didn't reach 20,000,000 Gold Coins." << endl << "Thank you for playing." << endl;
		}
	}

	if (earDistance == 0)
	{
		cout << "Your ear got driller by the Executioner." << endl
			<< "You have Failed." << endl
			<< "Shame on you and your cow." << endl;
	}

	system("pause");
	return 0;
}

//Fills Deck with Emperor Card
void fillEmperor(vector<string>& empVector)
{
	empVector.push_back("Emperor");
	empVector.push_back("Civilian");
	empVector.push_back("Civilian");
	empVector.push_back("Civilian");
	empVector.push_back("Civilian");

	cout << endl;
}

//Fills Deck with Slave Card
void fillSlave(vector<string>& slaVector)
{
	slaVector.push_back("Slave");
	slaVector.push_back("Civilian");
	slaVector.push_back("Civilian");
	slaVector.push_back("Civilian");
	slaVector.push_back("Civilian");

	cout << endl;
}

//Prints Player Deck ONLY
void printDeck(vector<string>& playerCardVector, int playerDeckSize)
{
		for (int i = 0; i < playerDeckSize; i++)
		{
			cout << "Card [ " << i + 1 << " ] :: " << playerCardVector[i] << endl;
		}
}

//Deck Distributor (FILL)
void distributeDeck(vector<string>& playerCardVector, vector<string>& aiCardVector, int matchRound)
{
	if (matchRound <= 3 || (matchRound >= 7 && matchRound <= 9))
	{
		cout << "Champion, you will be playing as the EMPEROR." << endl;
		fillEmperor(playerCardVector);
		fillSlave(aiCardVector);
	}
	else
	{
		cout << "Champion, you will be playing as the SLAVE." << endl;
		fillEmperor(aiCardVector);
		fillSlave(playerCardVector);
	}
}

//Player Picks Card
void playerCardPick(const vector<string>& playerCards, int plDeckSize, int userPick)
{
	if (userPick > plDeckSize)
	{
		cout << "ERROR 404: Card not Found." << endl;
	}
	else
	{
		cout << "You have chosen [ " << userPick << " ] :: " << playerCards.at(userPick - 1) << endl;
	}
}

//AI Picks Card
void computerCardPick(const vector<string>& aiCards, int aiDeckSize, int aiPick)
{
	srand(time(NULL));
	aiPick;
	cout << "Arthur Ino have chosen [ " << aiPick << " ] :: " << aiCards.at(aiPick - 1) << endl;
}

//Prints the Result of the Cards
void printVersus(const vector<string>& playerCards, const vector<string>& aiCards, int userPick, int aiPick)
{
	cout << "CHAMPION :: [ " << userPick << " ] [" << playerCards.at(userPick - 1) << "] .VS. [" << aiCards.at(aiPick - 1) << "] [ " << aiPick << " ] ::ARTHUR INO" << endl << endl;
}

//Prints the Winner through Decision
int printWinner(vector<string>& playerCards, vector<string>& aiCards, int userPick, int aiPick, int* currentGold, int* earDistance, int earBet)
{
	string plCardName = playerCards.at(userPick - 1);
	string aiCardName = aiCards.at(aiPick - 1);
	bool notDraw = 1;
	//DECISION ROUND
		//Player Deck is Emperor
		if (plCardName == "Emperor")
		{
			//Emperor > Civilian
			if (aiCardName == "Civilian")
			{
				cout << "[ " << plCardName << " ] Wins against [ " << aiCardName << " ]" << endl;
				*currentGold = *currentGold + (earBet * 100000);
				cout << "You have earned " << (earBet * 100000) << " Gold Coins. Congratulations!" << endl;
				cout << "Elexi Cushner's face isn't happy because she didn't drill a hole in to your ear." << endl;
			}
			//Emperor < Slave
			else if (aiCardName == "Slave")
			{
				cout << "[ " << plCardName << " ] Loses to [ " << aiCardName << " ]" << endl;
				*earDistance = *earDistance - earBet;
				cout << "Death has gotten ahold of your Emperor." << endl;
				cout << "Elexi Cushner drilled " << earBet << " Millimeters (mm) into your ear." << endl;
				cout << *earDistance << " Millimeters (mm) remains intact." << endl;
			}
		}

		//Player Deck is Slave
		else if (plCardName == "Slave")
		{
			//Slave < Civilian
			if (aiCardName == "Civilian")
			{
				cout << "[ " << plCardName << " ] Loses to [ " << aiCardName << " ]" << endl;
				*earDistance = *earDistance - earBet;
				cout << "Elexi Cushner drilled " << earBet << " Millimeters (mm) into your ear." << endl;
				cout << *earDistance << " Millimeters (mm) remains intact." << endl;
			}

			//Slave > Emperor
			else if (aiCardName == "Emperor")
			{
				cout << "[ " << plCardName << " ] Wins against [ " << aiCardName << " ]" << endl;
				*currentGold = *currentGold + (earBet * 500000);
				cout << "You have earned " << (earBet * 500000) << " Gold Coins (5x Multiplier!). Death to the Emperor!" << endl;
				cout << "Elexi Cushner's face isn't happy because she didn't drill a hole in to your ear." << endl;
			}
		}

		//Civilian Card
		else if (plCardName == "Civilian")
		{
			//Civilian = Civilian
			if (aiCardName == "Civilian")
			{
				cout << "Both cards beat each other. It is a DRAW!" << endl;
				cout << "[ " << plCardName << " ] [ " << aiCardName << " ]" << endl;
				cout << "Nothing happens." << endl;
				playerCards.erase(playerCards.begin() + (userPick - 1));
				aiCards.erase(aiCards.begin() + (aiPick - 1));
				//Deleting Entries
				notDraw = 0;
			}

			//Civilian > Slave
			else if (aiCardName == "Slave")
			{
				cout << "[ " << plCardName << " ] Wins against [ " << aiCardName << " ]" << endl;
				*currentGold = *currentGold + (earBet * 100000);
				cout << "You have earned " << (earBet * 100000) << " Gold Coins. Congratulations!" << endl;
				cout << "Elexi Cushner's face isn't happy because she didn't drill a hole in to your ear." << endl;
			}
		}
		return notDraw;
}
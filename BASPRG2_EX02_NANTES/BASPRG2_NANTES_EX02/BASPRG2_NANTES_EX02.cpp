#include <iostream>
#include <cmath>
#include <string>
#include <time.h>

using namespace std;

int main()
{
	//Declaration
	int goldAU = 1000;
	srand(time(NULL));
	int myBet;

	while (goldAU != 0)
	{
		
		//Player bets
		cout << "Current Gold : " << goldAU << endl;
		cout << "Enter bet here: ";
		cin >> myBet;
		cout << endl << myBet << " placed! Rolling Dice..." << endl << endl;



		int aisum = 0;
		int sum = 0;
		int aidiceOne = rand() % 6 + 1;
		int aidiceTwo = rand() % 6 + 1;
		int pldiceOne = rand() % 6 + 1;
		int pldiceTwo = rand() % 6 + 1;

		//Player Roll
		if (myBet <= goldAU)
		{

			pldiceOne;
			pldiceTwo;
			sum = pldiceOne + pldiceTwo;

			cout << "Player casually tosses his [Golden Dice]." << endl;
			cout << pldiceOne << " (1-6) [Golden Dice]" << endl;
			cout << pldiceTwo << " (1-6) [Golden Dice]" << endl;
			cout << endl << endl;

			//AI Roll

			aidiceOne;
			aidiceTwo;
			aisum = aidiceOne + aidiceTwo;

			cout << "Artificia casually tosses her [Mechanical Dice]." << endl;
			cout << aidiceOne << " (1-6) [Mechanical Dice]" << endl;
			cout << aidiceTwo << " (1-6) [Mechanical Dice]" << endl;
			cout << endl << endl;

			//Judgment
			if (sum > aisum)
			{
				goldAU = goldAU + (2 * myBet);
				cout << "Player wins!" << endl;
				cout << "Player receives " << (2 * myBet) << " gold." << endl;
				cout << "Current GOLD count : " << goldAU;
				cout << endl << endl;
			}
			else if (sum < aisum)
			{
				goldAU = goldAU - myBet;
				cout << "Artificia wins!" << endl;
				cout << "Player loses " << myBet << " gold." << endl;
				cout << "Current GOLD count : " << goldAU;
				cout << endl << endl;
			}

			//Snake-Eyes
			if (pldiceOne == 1 && pldiceTwo == 1)
			{
				if (aidiceOne != 1 && aidiceTwo != 1)
				{
					goldAU = goldAU + (3 * myBet);
					cout << "SSSSSSSSSnake Eyes! Player wins 3x of the bet." << endl;
					cout << "Player receives " << (3 * myBet) << " gold.";
					cout << "Current GOLD count : " << goldAU;
					cout << endl << endl;
				}
				else
				{
					goldAU = goldAU + myBet;
					cout << "SSSSSSSSSSnake Eyes! Both parties rolled the infamous Snake Eyes! It's a draw!" << endl;
					cout << "Current GOLD count : " << goldAU;
					cout << endl << endl;
				}
			}

			//Draw Mechanic
			if (sum == aisum)
			{
				goldAU = goldAU + myBet;
				cout << "It's a draw!" << endl;
				cout << "Player receives " << myBet << " gold." << endl;
				cout << "Current GOLD count : " << goldAU;
				cout << endl << endl;
			}
			

		}
		else
		{
			cout << "You do not have enough gold to bet like that! TRY AGAIN!";

		}

		if (goldAU == 0)
		{
			cout << "You have 0 gold left. GAME OVER." << endl;
			break;
		}
	}






	system("pause");
	return 0;

}
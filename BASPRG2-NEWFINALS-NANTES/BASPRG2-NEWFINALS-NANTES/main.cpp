#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>

#include "Unit.h"
#include "Item.h"
#include "Stats.h"
#include "HealthPotion.h"
#include "Crystals.h"
#include "Bomb.h"
#include "Rare.h"
using namespace std;

//Create Unit
Unit* createUnit()
{
	//Stats
	Stats stats;
	stats.hp = 100;
	stats.crystals = 100;
	stats.raritypoints = 0;

	//Unit
	Unit* player = new Unit("Dombuldore", stats);

	//Add Items and their Vector
	Item* healthpotion = new HealthPotion("Health Potion", 30);
	player->addItem(healthpotion);

	Item* bomb = new Bomb("Bomb", 25);
	player->addItem(bomb);

	Item* crystals = new Crystals("Crystals", 15);
	player->addItem(crystals);

	Item* r = new Rare("RARE", StatR, 1);
	player->addItem(r);

	Item* sr = new Rare("SUPER RARE", StatSR, 10);
	player->addItem(sr);

	Item* ssr = new Rare("ULTRA RARE", StatSSR, 50);
	player->addItem(ssr);

	return player;
}

int main()
{
	srand(time(NULL));

	//Counters
	int pulls = 0;
	int pullBomb = 0;
	int pullPotion = 0;
	int pullCrystal = 0;
	int pullR = 0;
	int pullSR = 0;
	int pullSSR = 0;

	
	Unit* unit = createUnit();
	
	//Win-Lose Conditions
	while (true)
	{
		//Display Stats
		unit->displayStatus();
		cout << "Pulls : " << pulls << endl;
		cout << endl;

		//Random
		int random = rand() % 100 + 1;
		random;

		//Item to be Activated - Loot Table - Conditions
		if (random >= 1 && random <= 14)
		{
			//Deduct Crystals -> Get Item -> Cast Item

			unit->takeCrystal(5);
			Item* item = unit->getItems()[2];
			item->activate(unit);
			pullCrystal++;
		}

		//Bomb
		else if (random >= 15 && random <= 34)
		{
			unit->takeCrystal(5);
			Item* item = unit->getItems()[1];
			item->activate(unit);
			pullBomb++;
		}

		//Health Potion
		else if (random >= 35 && random <= 49)
		{
			unit->takeCrystal(5);
			Item* item = unit->getItems()[0];
			item->activate(unit);
			pullPotion++;
		}

		//R
		else if (random >= 50 && random <= 89)
		{
			unit->takeCrystal(5);
			Item* item = unit->getItems()[3];
			item->activate(unit);
			pullR++;
		}

		//SR
		else if (random >= 90 && random <= 99)
		{
			unit->takeCrystal(5);
			Item* item = unit->getItems()[4];
			item->activate(unit);
			pullSR++;
		}

		//SSR
		else if (random == 100)
		{
			unit->takeCrystal(5);
			Item* item = unit->getItems()[5];
			item->activate(unit);
			pullSSR++;
		}

		system("pause");
		system("CLS");

		if (unit->getStats().crystals == 0) break;
		if (unit->getStats().hp == 0) break;
		if (unit->getStats().raritypoints == 100) break;

		pulls++;

	}


	//WIN-LOSE
	if (unit->getStats().hp == 0 || unit->getStats().crystals == 0)
	{
		cout << "YOU LOSE! You ran out of [Health] or you ran out of [Crystals]." << endl;
	}
	else if (unit->getStats().raritypoints)
	{
		cout << "YOU WIN! You have obtained 100 [Rarity Points]." << endl;
	}

	system("pause");
	cout << endl << endl;

	//Game Summary
	cout << " === GAME SUMMARY === " << endl;
	cout << "Pulls : " << pulls << endl
		<< "R : " << pullR << endl
		<< "SR : " << pullSR << endl
		<< "SSR : " << pullSSR << endl
		<< "Health Potions : " << pullPotion << endl
		<< "Bombs : " << pullBomb << endl
		<< "Crystals : " << pullCrystal << endl;

	system("pause");
	return 0;
}
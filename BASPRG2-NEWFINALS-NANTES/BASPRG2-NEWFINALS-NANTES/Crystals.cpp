#include "Crystals.h"

Crystals::Crystals(string name, int amount)
	:Item(name)
{
	mAmount = amount;
}

Crystals::~Crystals()
{
}

void Crystals::activate(Unit* target)
{
	Item::activate(target);
	target->addCrystal(mAmount);
	cout << target->getName() << " has obtained " << mAmount << " Crystals." << endl;
}

#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>

#include "Item.h"
using namespace std;

class Rare : public Item
{
private:
	int mPoints;
	StatType mType;

public:
	Rare(string name, StatType type, int points);
	~Rare();

	void activate(Unit* target);
};


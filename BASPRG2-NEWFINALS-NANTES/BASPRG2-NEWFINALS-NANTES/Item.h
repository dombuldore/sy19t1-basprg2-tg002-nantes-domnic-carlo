#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>

#include "Unit.h"
using namespace std;

class Unit;

class Item
{
private:
	string mName;
	Unit* mActor;
	int itemCost;

public:
	Item(string name);
	~Item();

	Unit* getActor();
	void setActor(Unit* actor);

	virtual void activate(Unit* target);
};


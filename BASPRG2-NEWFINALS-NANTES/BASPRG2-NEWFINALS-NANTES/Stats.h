#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>
using namespace std;

enum StatType
{
	StatR,
	StatSR,
	StatSSR
};

struct Stats
{
	int hp = 0;
	int crystals = 0;
	int raritypoints = 0;
};

void addStats(Stats& source, const Stats& toAdd);
void printStats(const Stats& stats);

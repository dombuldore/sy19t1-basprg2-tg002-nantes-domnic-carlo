#include "Rare.h"

Rare::Rare(string name, StatType type, int points)
	:Item(name)
{
	mType = type;
	mPoints = points;
}

Rare::~Rare()
{
}

void Rare::activate(Unit* target)
{
	Item::activate(target);
	string rareName;

	switch (mType)
	{
	case StatR:
		rareName = "R";
		target->getStats().raritypoints += mPoints;
		break;
	case StatSR:
		rareName = "SR";
		target->getStats().raritypoints += mPoints;
		break;
	case StatSSR:
		rareName = "SSR";
		target->getStats().raritypoints += mPoints;
		break;
	}

	cout << target->getName() << " has obtained " << rareName << " and has gained " << mPoints << " Rarity Points." << endl;
}

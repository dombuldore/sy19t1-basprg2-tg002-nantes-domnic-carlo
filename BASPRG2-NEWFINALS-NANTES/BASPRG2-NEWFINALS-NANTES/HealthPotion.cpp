#include "HealthPotion.h"

HealthPotion::HealthPotion(string name, int healValue)
	:Item(name)
{
	mHealValue = healValue;
}

HealthPotion::~HealthPotion()
{
}

void HealthPotion::activate(Unit* target)
{
	Item::activate(target);
	target->heal(mHealValue);
	cout << target -> getName() << " is healed for " << mHealValue << " Health." << endl;
}

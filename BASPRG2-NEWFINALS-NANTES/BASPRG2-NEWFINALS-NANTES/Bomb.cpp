#include "Bomb.h"

Bomb::Bomb(string name, int damageValue)
	:Item(name)
{
	mDamageAmount = damageValue;
}

Bomb::~Bomb()
{
}

void Bomb::activate(Unit* target)
{
	Item::activate(target);
	target->takeDamage(mDamageAmount);
	cout << target->getName() << " has been dealt " << mDamageAmount << " Damage." << endl;
}

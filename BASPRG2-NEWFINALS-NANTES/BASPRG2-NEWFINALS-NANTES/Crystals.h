#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>

#include "Item.h"
using namespace std;

 class Crystals : public Item
{
private:
	int mAmount;

public:
	Crystals(string name, int amount);
	~Crystals();

	void activate(Unit* target);
};


#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>

#include "Item.h"
using namespace std;

class Bomb : public Item
{
private:
	int mDamageAmount;

public:
	Bomb(string name, int damageValue);
	~Bomb();

	void activate(Unit* target);
};


#include "Item.h"

Item::Item(string name)
{
	mName = name;
}

Item::~Item()
{
}

Unit* Item::getActor()
{
	return mActor;
}

void Item::setActor(Unit* actor)
{
	mActor = actor;
}

void Item::activate(Unit* target)
{
	if (mActor == NULL || target == NULL) throw exception("Actor or Target cannot be NULL!");
	cout << mActor->getName() << " pulled " << mName << endl;
	//progamermove lmao
}

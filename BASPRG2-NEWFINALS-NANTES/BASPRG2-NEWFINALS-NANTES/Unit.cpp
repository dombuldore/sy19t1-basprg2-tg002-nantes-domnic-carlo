#include "Unit.h"

Unit::Unit(string name, const Stats& stats)
{
	mName = name;
	mStats = stats;

	//to test heal
}

Unit::~Unit()
{
}

string Unit::getName()
{
	return mName;
}

Stats& Unit::getStats()
{
	return mStats;
}

int Unit::getCurrentHp()
{
	return getStats().hp;
}

const vector<Item*>& Unit::getItems()
{
	return mItems;
}

void Unit::addItem(Item* item)
{
	item->setActor(this);
	mItems.push_back(item);
}

bool Unit::alive()
{
	return getStats().hp > 0;
}

void Unit::heal(int amount)
{
	if (amount < 1) throw new exception("Amount cannot be lower than 1!");
	getStats().hp += amount;
}

void Unit::addCrystal(int amount)
{
	getStats().crystals += amount;
}

void Unit::takeCrystal(int amount)
{
	//Deduct Crystals
	getStats().crystals -= amount;
}

void Unit::takeDamage(int damage)
{
	if (damage < 0) return;
	getStats().hp -= damage;
	if (getStats().hp < 0) getStats().hp = 0;
}

void Unit::displayStatus()
{
	cout << "NAME : " << mName << endl;
	cout << "HEALTH : " << mStats.hp << endl;
	cout << "CRYSTALS : " << mStats.crystals << endl;
	cout << "RARITY POINTS : " << mStats.raritypoints << endl;
}

#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>

#include "Item.h"
using namespace std;

class HealthPotion : public Item
{
private:
	int mHealValue;

public:
	HealthPotion(string name, int healValue);
	~HealthPotion();

	void activate(Unit* target);
};


#include "Stats.h"

void addStats(Stats& source, const Stats& toAdd)
{
	source.hp += toAdd.hp;
	source.crystals += toAdd.crystals;
	source.raritypoints += toAdd.raritypoints;
}

void printStats(const Stats& stats)
{
	cout << "HEALTH : " << stats.hp << endl;
	cout << "CRYSTALS : " << stats.crystals << endl;
	cout << "RARITY POINTS : " << stats.raritypoints << endl;
}

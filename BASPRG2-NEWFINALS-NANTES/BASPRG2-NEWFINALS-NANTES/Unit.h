#pragma once
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <cmath>

#include "Stats.h"
#include "Item.h"
using namespace std;

class Item;

class Unit
{
private:
	string mName;
	Stats mStats;
	int mCurrentHp;
	int mCurrentCrystals;
	vector<Item*> mItems;

public:
	Unit(string name, const Stats& stats);
	~Unit();

	string getName();
	Stats& getStats();
	int getCurrentHp();

	//Item Vector
	const vector<Item*>& getItems();
	void addItem(Item* item);

	//Other Stuff
	bool alive();
	void heal(int amount);
	void addCrystal(int amount);
	void takeCrystal(int amount);
	void takeDamage(int damage);
	void displayStatus();
};


#include <iostream>
using namespace std;


float nFactorialTwo(float n)
{
	if (n == 0 || n == 1)
	{
		return 1;
	}
	else
	{
		for (int i = 1; i <= n; i++)
		{
			float result = n * nFactorialTwo(n - 1);
			return result;
		}
	}
}
long nFactorial(long n)
{
	if (n == 0 || n == 1)
	{
		return 1;
	}
	else
	{
		long result = n * nFactorial(n - 1);
		return result;
	}
	//source : https://www.tutorialspoint.com/cplusplus-program-to-find-factorial-of-a-number-using-recursion
	//WILL ALSO TRY THE FOR LOOP -
}



int main()
{
	int x;
	cout << "Enter Number : ";
	cin >> x;

	cout << " == IF-ELSE CODE CHUNK == " << endl;
	cout << "Factorial of " << x << " is : " << nFactorial(x) << endl;

	cout << " == FOR LOOP CODE CHUNK == " << endl;
	cout << "Factorial of " << x << " is : " << nFactorialTwo(x) << endl;
	system("pause");
	return 0;
}


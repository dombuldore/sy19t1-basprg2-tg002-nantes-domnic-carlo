#include <iostream>
#include <string>
using namespace std;

int main()
{
	//See CheatCodes for easterEgg
	string decision;
	int userGold = 250;
	int packageReward[7];
	packageReward[0] = 30750;
	packageReward[1] = 1780;
	packageReward[2] = 500;
	packageReward[3] = 250000;
	packageReward[4] = 13333;
	packageReward[5] = 150;
	packageReward[6] = 4050;
	int userPrice;

	while (true)
	{
		//sorting packageList (SORTING ALGORITHM)
		int x;
		for (int i = 0; i < 7; i++)
		{
			for (int j = i + 1; j < 7; j++)
			{
				if (packageReward[i] > packageReward[j])
				{
					x = packageReward[i];
					packageReward[i] = packageReward[j];
					packageReward[j] = x;
				}
			}
		}

		cout << "WELCOME TO THE 'IN-APP PURCHASING' GAME!" << endl;
		cout << ":: SORTED PACKAGE LIST ::" << endl << endl;
		//displaying the Sorted List of the Array
		for (int k = 0; k < 7; k++)
		{
			cout << "Package [ " << k << " ] :: " << packageReward[k] << endl;
		}

		//user Input
		cout << endl << endl << "Current Gold :: " << userGold << endl;
		cout << "Enter Price here :: ";
		cin >> userPrice;
	
			//case One
			if (userGold >= userPrice)
			{
				cout << endl;
				userGold = userGold - userPrice;
				cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
				cout << "You only have " << userGold << " left in your bag." << endl;
			}

			//case Two and Three
			//checks userInput and see if the sum of userGold + package[n] will be enough to buy the the userInput
			//failed to convert this to a function.
			else if (userGold < userPrice)
			{
				if (userGold + packageReward[0] >= userPrice)
				{
					userGold = userGold + packageReward[0];
					cout << packageReward[0] << " GOLD has been added to your bag. Thank you for purchasing PACKAGE [0]." << endl;
					cout << "You now have " << userGold << " GOLD in your bag." << endl;
					userGold = userGold - userPrice;
					cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
					cout << "You only have " << userGold << " left in your bag." << endl;
				}
				else if (userGold + packageReward[1] >= userPrice)
				{
					userGold = userGold + packageReward[1];
					cout << packageReward[1] << " GOLD has been added to your bag. Thank you for purchasing PACKAGE [1]." << endl;
					cout << "You now have " << userGold << " GOLD in your bag." << endl;
					userGold = userGold - userPrice;
					cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
					cout << "You only have " << userGold << " left in your bag." << endl;
				}
				else if (userGold + packageReward[2] >= userPrice)
				{
					userGold = userGold + packageReward[2];
					cout << packageReward[2] << " GOLD has been added to your bag. Thank you for purchasing PACKAGE [2]." << endl;
					cout << "You now have " << userGold << " GOLD in your bag." << endl;
					userGold = userGold - userPrice;
					cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
					cout << "You only have " << userGold << " left in your bag." << endl;
				}
				else if (userGold + packageReward[3] >= userPrice)
				{
					userGold = userGold + packageReward[3];
					cout << packageReward[3] << " GOLD has been added to your bag. Thank you for purchasing PACKAGE [3]." << endl;
					cout << "You now have " << userGold << " GOLD in your bag." << endl;
					userGold = userGold - userPrice;
					cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
					cout << "You only have " << userGold << " left in your bag." << endl;
				}
				else if (userGold + packageReward[4] >= userPrice)
				{
					userGold = userGold + packageReward[4];
					cout << packageReward[4] << " GOLD has been added to your bag. Thank you for purchasing PACKAGE [4]." << endl;
					cout << "You now have " << userGold << " GOLD in your bag." << endl;
					userGold = userGold - userPrice;
					cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
					cout << "You only have " << userGold << " left in your bag." << endl;
				}
				else if (userGold + packageReward[5] >= userPrice)
				{
					userGold = userGold + packageReward[5];
					cout << packageReward[5] << " GOLD has been added to your bag. Thank you for purchasing PACKAGE [5]." << endl;
					cout << "You now have " << userGold << " GOLD in your bag." << endl;
					userGold = userGold - userPrice;
					cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
					cout << "You only have " << userGold << " left in your bag." << endl;
				}
				else if (userGold + packageReward[6] >= userPrice)
				{
					userGold = userGold + packageReward[6];
					cout << packageReward[6] << " GOLD has been added to your bag. Thank you for purchasing PACKAGE [6]." << endl;
					cout << "You now have " << userGold << " GOLD in your bag." << endl;
					userGold = userGold - userPrice;
					cout << userPrice << " GOLD has been deducted from your bag. Thank you for your purchase!" << endl;
					cout << "You only have " << userGold << " left in your bag." << endl;
				}
				//case Three
				else
				{
					cout << "You trying to be funny? We don't have that kind of Package." << endl;
				}
			}

	
		//ending Block
		cout << endl << endl;
		cout << "Would you like to continue shopping? [Y / N] :: ";
		cin >> decision;
		system("CLS");

		//[CHEATCODES] BELLA CIAO BELLA CIAO BELLA CIAO CIAO CIAO
		if (decision == "ELPROFESSOR")
		{
			cout << " $$$ BELLA CIAO BELLA CIAO BELLA CIAO CIAO CIAO $$$$" << endl;
			userGold = userGold + 961000000;
		}
		//EXIT BLOCK
		if (decision == "n")
		{
			break;
		}
	}

	cout << "Thank you for playing! Come back again soon!" << endl << endl;

	system("pause");
	return 0;
}
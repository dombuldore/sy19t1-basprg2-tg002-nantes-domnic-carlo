#include <iostream>
#include <string>
#include <time.h>
using namespace std;

void printArray(int randoNumbers[10]);

int main()
{
	int randoNumber[10] = { rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, rand() % 100 + 1, };
	printArray(randoNumber);

	//source : https://www.javatpoint.com/cpp-passing-array-to-function
	system("pause");
	return 0;
}

void printArray(int randoNumbers[10])
{
	cout << "Array" << endl;
	for (int i = 0; i <= 9; i++)
	{
		cout << randoNumbers[i] << endl;
	}
}
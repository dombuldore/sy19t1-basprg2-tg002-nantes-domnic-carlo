#include <iostream>
using namespace std;

int main()
{
	int numArr[10];
	int n = 10;
	int i = 0;


	for (i = 0; i < n; i++)
	{
		cout << "Enter Number [" << (i + 1) << "] : ";
		cin >> numArr[i];
	}
	cout << endl << endl;

	//Displaying the Array
	for (int j = 0; j <= n; j++)
	{
		if (j == 0)
		{
			cout << "Array :: {" << numArr[j] << " , ";
			continue;
		}
		if (j == 9)
		{
			cout << numArr[j] << " }" << endl << endl;
			break;
		}
		else
		{
			cout << numArr[j] << " , ";
		}
	}
	cout << endl;
	cout << "Sorting Array now..." << endl << endl;


	//Arrangement
	int temp;
	int minimum;
	for (int k = n - 1; k > 0; k--)
	{
		minimum = 0;
		for (int l = 1; l <= k; l++)
		{
			if (numArr[l] > numArr[minimum])
			{
				minimum = l;
			}

		}
		temp = numArr[minimum];
		numArr[minimum] = numArr[k];
		numArr[k] = temp;
	}

	//Displaying Sorted Array
	for (int m = 0; m < n; m++)
	{
		if (m == 0)
		{
			cout << "SORTED Array :: {" << numArr[m] << " , ";
			continue;
		}
		if (m == 9)
		{
			cout << numArr[m] << " }" << endl << endl;
			break;
		}
		else
		{
			cout << numArr[m] << " , ";
		}

	}



	system("pause");
	return 0;
}
#pragma once
//Header - Function Declarations
//Source -- > https://www.youtube.com/watch?v=vz1O9nRyZaY
#include <iostream>
#include <string>

using namespace std;

#ifndef cloudStrife_H
#define cloudStrife_H

class characterCloudStrife
{
public:
	//Default Constructor
	characterCloudStrife();

	//Mutator Functions
	//---------------------------------- GETS
	string getCharacterName() const;
	int getCharacterLevel() const;
	int getCharacterExp() const;
	int getCharacterCurrentHealth() const;
	int getCharacterMaxHealth() const;
	int getCharacterCurrentMana() const;
	int getCharacterMaxMana() const;

	int getCharacterStr() const;
	int getCharacterDex() const;
	int getCharacterVit() const;
	int getCharacterMagic() const;
	int getCharacterSpirit() const;
	int getCharacterLuck() const;

	int getCharacterAttack() const;
	int getCharacterAttChance() const;
	int getCharacterDefense() const;
	int getCharacterDefChance() const;
	int getCharacterMagicAtt() const;
	int getCharacterMagicDef() const;
	int getCharacterMagicDefChance() const;



	//---------------------------------- VOIDS
	void setCharacterName(string);
	void setCharacterLevel(int);
	void setCharacterCurrentHealth(int);
	void setCharacterMaxHealth(int);
	void setCharacterCurrentMana(int);
	void setCharacterMaxMana(int);
	void setCharacterExp(int);
	
	void setCharacterStr(int);
	void setCharacterDex(int);
	void setCharacterVit(int);
	void setCharacterMagic(int);
	void setCharacterSpirit(int);
	void setCharacterLuck(int);

	void setCharacterAttack(int);
	void setCharacterAttChance(int);
	void setCharacterDefense(int);
	void setCharacterDefChance(int);
	void setCharacterMagicAtt(int);
	void setCharacterMagicDef(int);
	void setCharacterMagicDefChance(int);


private:
	//Member Variables
	string newCharacterName;
	int newCharacterLevel;
	int newCharacterExp;
	int newCharacterCurrentHealth;
	int newCharacterMaxHealth;
	int newCharacterCurrentMana;
	int newCharacterMaxMana;
	
	int newCharacterStr;
	int newCharacterDex;
	int newCharacterVit;
	int newCharacterMagic;
	int newCharacterSpirit;
	int newCharacterLuck;
	
	int newCharacterAttack;
	int newCharacterAttChance;
	int newCharacterDefense;
	int newCharacterDefChance;
	int newCharacterMagicAtt;
	int newCharacterMagicDef;
	int newCharacterMagicDefChance;



};

#endif

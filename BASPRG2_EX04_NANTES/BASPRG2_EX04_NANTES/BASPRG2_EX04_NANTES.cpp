#include <iostream>
#include <string>

#include "cloudStrife.h"

using namespace std;

int main()
{
	string characterName;
	int characterLevel;
	int characterHealth;
	int characterCurrentHealth;
	int characterMana;
	int characterCurrentMana;
	int characterExp;

	int characterStr;
	int characterDex;
	int characterVit;
	int characterMagic;
	int characterSpirit;
	int characterLuck;

	int characterAttack;
	int characterAttChance;
	int characterDefense;
	int characterDefChance;
	int characterMagicAtt;
	int characterMagicDef;
	int characterMagicDefChance;

	string characterWeapon;
	string characterArmor;
	string characterAccessories;
	 
	//Following the JPG Image of Cloud Strife
	cout << "=== MAIN STUFF ===" << endl;
	cout << "Enter Character Name :: ";
	cin >> characterName;
	cout << "Enter Character Level :: ";
	cin >> characterLevel;
	cout << "Enter Character Exp :: ";
	cin >> characterExp;
	cout << "Enter Character Current Health :: ";
	cin >> characterCurrentHealth;
	cout << "Enter Character Max Health :: ";
	cin >> characterHealth;
	cout << "Enter Character Current Mana :: ";
	cin >> characterCurrentMana;
	cout << "Enter Character Max Mana :: ";
	cin >> characterMana;

	cout << "=== STATS ===" << endl;
	cout << "Enter Character Str :: ";
	cin >> characterStr;
	cout << "Enter Character Dex :: ";
	cin >> characterDex;
	cout << "Enter Character Vit :: ";
	cin >> characterVit;
	cout << "Enter Character Magic :: ";
	cin >> characterMagic;
	cout << "Enter Character Spirit :: ";
	cin >> characterSpirit;
	cout << "Enter Character Luck :: ";
	cin >> characterLuck;

	cout << "=== MORE STATS ===" << endl;
	cout << "Enter Character Attack :: ";
	cin >> characterAttack;
	cout << "Enter Character Attack Chance :: ";
	cin >> characterAttChance;
	cout << "Enter Character Defense :: ";
	cin >> characterDefense;
	cout << "Enter Character Defense Chance :: ";
	cin >> characterDefChance;
	cout << "Enter Character Magic Attack :: ";
	cin >> characterMagicAtt;
	cout << "Enter Character Magic Defense :: ";
	cin >> characterMagicDef;
	cout << "Enter Character Magic Defense Chance :: ";
	cin >> characterMagicDefChance;

	characterCloudStrife cloud_strife;
	cloud_strife.setCharacterName(characterName);
	cloud_strife.setCharacterLevel(characterLevel);
	cloud_strife.setCharacterExp(characterExp);
	cloud_strife.setCharacterCurrentHealth(characterCurrentHealth);
	cloud_strife.setCharacterMaxHealth(characterHealth);
	cloud_strife.setCharacterCurrentMana(characterCurrentMana);
	cloud_strife.setCharacterMaxMana(characterMana);

	cloud_strife.setCharacterStr(characterStr);
	cloud_strife.setCharacterDex(characterDex);
	cloud_strife.setCharacterVit(characterVit);
	cloud_strife.setCharacterMagic(characterMagic);
	cloud_strife.setCharacterSpirit(characterSpirit);
	cloud_strife.setCharacterLuck(characterLuck);

	cloud_strife.setCharacterAttack(characterAttack);
	cloud_strife.setCharacterAttChance(characterAttChance);
	cloud_strife.setCharacterDefense(characterDefense);
	cloud_strife.setCharacterDefChance(characterDefChance);
	cloud_strife.setCharacterMagicAtt(characterMagicAtt);
	cloud_strife.setCharacterMagicDef(characterMagicDef);
	cloud_strife.setCharacterMagicDefChance(characterMagicDefChance);

	system("CLS");

	cout << "==== CHARACTER STATS ====" << endl;
	cout << "Character Name :: " << cloud_strife.getCharacterName() << endl
		<< "Level : " << cloud_strife.getCharacterLevel() << endl
		<< "HP " << cloud_strife.getCharacterCurrentHealth() << " / " << cloud_strife.getCharacterMaxHealth() << endl
		<< "MP " << cloud_strife.getCharacterCurrentMana() << " / " << cloud_strife.getCharacterMaxMana() << endl
		<< endl
		<< "Strength : " << cloud_strife.getCharacterStr() << endl
		<< "Dexterity : " << cloud_strife.getCharacterDex() << endl
		<< "Vitality : " << cloud_strife.getCharacterVit() << endl
		<< "Magic : " << cloud_strife.getCharacterMagic() << endl
		<< "Spirit : " << cloud_strife.getCharacterSpirit() << endl
		<< "Luck : " << cloud_strife.getCharacterLuck() << endl
		<< endl
		<< "Attack : " << cloud_strife.getCharacterAttack() << endl
		<< "Attack % : " << cloud_strife.getCharacterAttChance() << endl
		<< "Defense : " << cloud_strife.getCharacterDefense() << endl
		<< "Magic Attack : " << cloud_strife.getCharacterMagicAtt() << endl
		<< "Magic Defense : " << cloud_strife.getCharacterMagicDef() << endl
		<< "Magic Defense % : " << cloud_strife.getCharacterMagicDefChance() << endl;

	cout << endl << endl;








	
	 

	system("pause");
	return 0;
}
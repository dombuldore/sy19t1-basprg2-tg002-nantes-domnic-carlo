//Function Definitions
#include "cloudStrife.h"

characterCloudStrife::characterCloudStrife()
{
	newCharacterLevel = 0;
	newCharacterCurrentHealth = 0;
	newCharacterMaxHealth = 0;
	newCharacterCurrentMana = 0;
	newCharacterMaxMana = 0;
	newCharacterExp = 0;

	newCharacterStr = 0;
	newCharacterDex = 0;
	newCharacterVit = 0;
	newCharacterMagic = 0;
	newCharacterSpirit = 0;
	newCharacterLuck = 0;

	newCharacterAttack = 0;
	newCharacterAttChance = 0;
	newCharacterDefense = 0;
	newCharacterDefChance = 0;
	newCharacterMagicAtt = 0;
	newCharacterMagicDef = 0;
	newCharacterMagicDefChance = 0;
}

//------------------------------------------------------------------------------------ GETS


string characterCloudStrife::getCharacterName() const
{
	return newCharacterName;
}
int characterCloudStrife::getCharacterLevel() const
{
	return newCharacterLevel;
}
int characterCloudStrife::getCharacterCurrentHealth() const
{
	return newCharacterCurrentHealth;
}
int characterCloudStrife::getCharacterMaxHealth() const
{
	return newCharacterMaxHealth;
}
int characterCloudStrife::getCharacterCurrentMana() const
{
	return newCharacterCurrentMana;
}
int characterCloudStrife::getCharacterMaxMana() const
{
	return newCharacterMaxMana;
}
int characterCloudStrife::getCharacterExp() const
{
	return newCharacterExp;
}


int characterCloudStrife::getCharacterStr() const
{
	return newCharacterStr;
}
int characterCloudStrife::getCharacterDex() const
{
	return newCharacterDex;
}
int characterCloudStrife::getCharacterVit() const
{
	return newCharacterVit;
}
int characterCloudStrife::getCharacterMagic() const
{
	return newCharacterMagic;
}
int characterCloudStrife::getCharacterSpirit() const
{
	return newCharacterSpirit;
}
int characterCloudStrife::getCharacterLuck() const
{
	return newCharacterLuck;
}


int characterCloudStrife::getCharacterAttack() const
{
	return newCharacterAttack;
}
int characterCloudStrife::getCharacterAttChance() const
{
	return newCharacterAttChance;
}
int characterCloudStrife::getCharacterDefense() const
{
	return newCharacterDefense;
}
int characterCloudStrife::getCharacterDefChance() const
{
	return newCharacterDefChance;
}
int characterCloudStrife::getCharacterMagicAtt() const
{
	return newCharacterMagicAtt;
}
int characterCloudStrife::getCharacterMagicDef() const
{
	return newCharacterMagicDef;
}
int characterCloudStrife::getCharacterMagicDefChance() const
{
	return newCharacterMagicDefChance;
}

//------------------------------------------------------------------------------------ VOIDS

void characterCloudStrife::setCharacterName(string characterName)
{
	newCharacterName = characterName;
}
void characterCloudStrife::setCharacterLevel(int characterLevel)
{
	newCharacterLevel = characterLevel;
}
void characterCloudStrife::setCharacterCurrentHealth(int characterCurrentHealth)
{
	newCharacterCurrentHealth = characterCurrentHealth;
}
void characterCloudStrife::setCharacterMaxHealth(int characterMaxHealth)
{
	newCharacterMaxHealth = characterMaxHealth;
}
void characterCloudStrife::setCharacterCurrentMana(int characterCurrentMana)
{
	newCharacterCurrentMana = characterCurrentMana;
}
void characterCloudStrife::setCharacterMaxMana(int characterMaxMana)
{
	newCharacterMaxMana = characterMaxMana;
}
void characterCloudStrife::setCharacterExp(int characterExp)
{
	newCharacterExp = characterExp;
}


void characterCloudStrife::setCharacterStr(int characterStr)
{
	newCharacterStr = characterStr;
}
void characterCloudStrife::setCharacterDex(int characterDex)
{
	newCharacterDex = characterDex;
}
void characterCloudStrife::setCharacterVit(int characterVit)
{
	newCharacterVit = characterVit;
}
void characterCloudStrife::setCharacterMagic(int characterMagic)
{
	newCharacterMagic = characterMagic;
}
void characterCloudStrife::setCharacterSpirit(int characterSpirit)
{
	newCharacterSpirit = characterSpirit;
}
void characterCloudStrife::setCharacterLuck(int characterLuck)
{
	newCharacterLuck = characterLuck;
}


void characterCloudStrife::setCharacterAttack(int characterAttack)
{
	newCharacterAttack = characterAttack;
}
void characterCloudStrife::setCharacterAttChance(int characterAttChance)
{
	newCharacterAttChance = characterAttChance;
}
void characterCloudStrife::setCharacterDefense(int characterDefense)
{
	newCharacterDefense = characterDefense;
}
void characterCloudStrife::setCharacterDefChance(int characterDefChance)
{
	newCharacterDefChance = characterDefChance;
}
void characterCloudStrife::setCharacterMagicAtt(int characterMagicAtt)
{
	newCharacterMagicAtt = characterMagicAtt;
}
void characterCloudStrife::setCharacterMagicDef(int characterMagicDef)
{
	newCharacterMagicDef = characterMagicDef;
}
void characterCloudStrife::setCharacterMagicDefChance(int characterMagicDefChance)
{
	newCharacterMagicDefChance = characterMagicDefChance;
}
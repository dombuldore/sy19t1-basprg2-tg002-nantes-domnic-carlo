#include "Warrior.h"
#include "Assassin.h"
#include "Mage.h"
#include <time.h>

Warrior::Warrior(string name)
{
	mName = name;
	mHp = 35;
	mMaxHp = 35;
	mPower = 10;
	mVit = 3;
	mDex = 11;
	mAgi = 11;
}

Warrior::~Warrior()
{
}

string Warrior::getName()
{
	return mName;
}

int Warrior::getHp()
{
	return mHp;
}

int Warrior::getMaxHp()
{
	return mMaxHp;
}

int Warrior::getPower()
{
	return mPower;
}

int Warrior::getVit()
{
	return mVit;
}

int Warrior::getAgi()
{
	return mAgi;
}

int Warrior::getDex()
{
	return mDex;
}

void Warrior::takeDamage(int damage)
{
	if (damage < 1) return;
	mHp -= damage; // No Bonus Damage
	if (mHp < 0) mHp = 0;
}

void Warrior::takeBonusDamage(int damage)
{
	if (damage < 1) return;
	mHp = mHp - (damage * 1.5); // Bonus Damage
	if (mHp < 0) mHp = 0;
}

void Warrior::attack(Warrior* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()); //*bonusDamage
		if (damage < 0) damage = 1;
		target->takeDamage(damage);
		cout << getName() << " deals " << damage << " damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Warrior::bonusAttack(Assassin* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()) * 1.5; // Bonus Damage
		if (damage < 0) damage = 1;
		target->takeBonusDamage(damage);
		cout << getName() << " deals " << damage << " bonus damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Warrior::attack(Mage* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()); //*bonusDamage
		if (damage < 0) damage = 1;
		target->takeDamage(damage);
		cout << getName() << " deals " << damage << " damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Warrior::heal()
{
	mHp = mHp * 1.3;
}

int Warrior::addStats()
{
	mMaxHp = mMaxHp + 3;
	mVit = mVit + 3;
	return mHp, mVit;
}

void Warrior::isStronger(int value)
{
	mHp = mHp * 1.4;
	mMaxHp = mMaxHp * 1.4;
	mPower = mPower * value;
	mVit = mVit * value;
	mAgi = mAgi * value;
	mDex = mDex * value;
}

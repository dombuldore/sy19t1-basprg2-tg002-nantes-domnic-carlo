#include "Assassin.h"
#include "Mage.h"
#include "Warrior.h"

Assassin::Assassin(string name)
{
	mName = name;
	mHp = 33;
	mMaxHp = 33;
	mPower = 5;
	mVit = 3;
	mAgi = 11;
	mDex = 11;
}

Assassin::~Assassin()
{
}

string Assassin::getName()
{
	return mName;
}

int Assassin::getHp()
{
	return mHp;
}

int Assassin::getMaxHp()
{
	return mMaxHp;
}


int Assassin::getPower()
{
	return mPower;
}

int Assassin::getVit()
{
	return mVit;
}

int Assassin::getAgi()
{
	return mAgi;
}

int Assassin::getDex()
{
	return mDex;
}

void Assassin::takeDamage(int damage)
{
	if (damage < 1) return;
	mHp -= damage; //*bonusDamageReceived
	if (mHp < 0) mHp = 0;
}

void Assassin::takeBonusDamage(int damage)
{
	if (damage < 1) return;
	mHp = mHp - (damage * 1.5); // Bonus Damage
	if (mHp < 0) mHp = 0;
}

void Assassin::attack(Assassin* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()) * 1.5; // Bonus Damage
		if (damage < 0) damage = 1;
		target->takeDamage(damage);
		cout << getName() << " deals " << damage << " damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Assassin::bonusAttack(Mage* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()) * 1.5; // Bonus Damage
		if (damage < 0) damage = 1;
		target->takeBonusDamage(damage);
		cout << getName() << " deals " << damage << " bonus damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Assassin::attack(Warrior* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()) * 1.5; // Bonus Damage
		if (damage < 0) damage = 1;
		target->takeDamage(damage);
		cout << getName() << " deals " << damage << " damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Assassin::heal()
{
	mHp = mHp * 1.3;
}

int Assassin::addStats()
{
	mAgi += 3;
	mDex += 3;
	return mAgi, mDex;
}

void Assassin::isStronger(int value)
{
	mHp = mHp * 1.4;
	mMaxHp = mMaxHp * 1.4;
	mPower = mPower * value;
	mVit = mVit * value;
	mAgi = mAgi * value;
	mDex = mDex * value;
}

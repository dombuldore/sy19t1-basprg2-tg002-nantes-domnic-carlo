#pragma once
#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
using namespace std;

class Warrior;
class Mage;

class Assassin
{
public:
	Assassin(string name);
	~Assassin();

	//Get
	string getName();
	int getHp();
	int getMaxHp();

	int getPower();
	int getVit();
	int getAgi();
	int getDex();

	//Voids
	void takeDamage(int damage);
	void takeBonusDamage(int damage);

	void attack(Assassin* target); // ASSASSIN - ASSASSIN
	void bonusAttack(Mage* target); // ASSASIN - MAGE (bonus damage)
	void attack(Warrior* target); // ASSASSIN - WARRIOR (take bonus damage)

	//Heal-On-Win
	void heal();

	//Add Stats `On Win
	int addStats();

	//Ai Get Stronger
	void isStronger(int value);


private:
	string mName;
	int mHp;
	int mMaxHp;
	int mPower;
	int mVit;
	int mAgi;
	int mDex;
	const int bonusDamage = 1.5;
};


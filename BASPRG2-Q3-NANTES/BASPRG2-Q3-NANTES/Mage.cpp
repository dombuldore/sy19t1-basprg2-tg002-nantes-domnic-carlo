#include "Mage.h"
#include "Warrior.h"
#include "Assassin.h"

Mage::Mage(string name)
{
	mName = name;
	mHp = 30;
	mMaxHp = 30;
	mPower = 10;
	mVit = 3;
	mAgi = 11;
	mDex = 11;
}

Mage::~Mage()
{
}

string Mage::getName()
{
	return mName;
}

int Mage::getHp()
{
	return mHp;
}

int Mage::getMaxHp()
{
	return mMaxHp;
}

int Mage::getPower()
{
	return mPower;
}

int Mage::getVit()
{
	return mVit;
}

int Mage::getAgi()
{
	return mAgi;
}

int Mage::getDex()
{
	return mDex;
}

void Mage::takeDamage(int damage)
{
	if (damage < 1) return;
	mHp -= damage; // No Bonus Damage
	if (mHp < 0) mHp = 0;
}

void Mage::takeBonusDamage(int damage)
{
	if (damage < 1) return;
	mHp = mHp - (damage * 1.5); // Bonus Damage
	if (mHp < 0) mHp = 0;
}

void Mage::attack(Mage* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit());
		if (damage < 0) damage = 1;
		target->takeDamage(damage);
		cout << getName() << " deals " << damage << " damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Mage::bonusAttack(Warrior* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()) * 1.5; // Bonus Damage
		if (damage < 0) damage = 1;
		target->takeBonusDamage(damage);
		cout << getName() << " deals " << damage << " bonus damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Mage::attack(Assassin* target)
{
	//int hitRate = rand() % ((getDex() % target->getAgi()) * 100) + 1;
	int hitRate = rand() % 100 + 1;
	if (hitRate <= 20 || hitRate >= 80)
	{
		cout << getName() << "'s misses!" << endl;
		return;
	}
	else
	{
		int damage = (getPower() - target->getVit()); //*bonusDamage
		if (damage < 0) damage = 1;
		target->takeDamage(damage);
		cout << getName() << " deals " << damage << " damage to " << target->getName() << "." << endl;
		cout << target->getName() << " 's remaining health :: " << target->getHp() << " / " << target->getMaxHp() << endl;
	}
}

void Mage::heal()
{
	mHp = mHp * 1.3;
}

int Mage::addStats()
{
	mPower += 5;
	return mPower;
}

void Mage::isStronger(int value)
{
	mHp = mHp * 1.4;
	mMaxHp = mMaxHp * 1.4;
	mPower = mPower * value;
	mVit = mVit * value;
	mAgi = mAgi * value;
	mDex = mDex * value;
}


#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <time.h>

#include "Assassin.h"
#include "Warrior.h"
#include "Mage.h"

using namespace std;
//Fill Vector
void fillVector(vector<string>& characterClass)
{
	characterClass.push_back("ASSASSIN");
	characterClass.push_back("MAGE");
	characterClass.push_back("WARRIOR");
}

//Print Vector
void printVector(vector<string>& characterClass)
{
	for (int i = 0; i < characterClass.size(); i++)
	{
		cout << "       [ " << i + 1 << " ] " << characterClass[i] << endl;
	}
}

void playerSpawn(vector<string>& characterClass, int choice)
{
	if (choice == 1)
	{
		system("CLS");
		cout << "You have chosen " << characterClass[0] << endl;
	}
	else if (choice == 2)
	{
		system("CLS");
		cout << "You have chosen " << characterClass[1] << endl;
	}
	else if (choice == 3)
	{
		system("CLS");
		cout << "You have chosen " << characterClass[2] << endl;
	}
	else
	{
		system("CLS");
		cout << "Enter a valid character class!" << endl;
		system("pause");
		system("CLS");
		abort();
	}

}

//Random AI Spawner
void randomSpawn(vector<string>& characterClass, int aiDice)
{
	if (aiDice == 0)
	{
		cout << "AI has chosen " << characterClass[0] << endl;
	}
	else if (aiDice == 1)
	{
		cout << "AI has chosen " << characterClass[1] << endl;
	}
	else
	{
		cout << "AI has chosen " << characterClass[2] << endl;
	}
}


int main()
{
	srand(time(NULL));
	vector<string> playerCharClass;
	vector<string> aiCharClass;
	int playerChoice;
	string plname;
	string aiName = "Arturito";
	int aiChoice = rand() % 3;
	int gameRound = 0;

	cout << "Enter your character name :: ";
	cin >> plname;
	system("CLS");

	//Character Creation
	fillVector(playerCharClass);
	Warrior* plWarrior = new Warrior(plname);
	Mage* plMage = new Mage(plname);
	Assassin* plAssassin = new Assassin(plname);
	 
	cout << "Choose your Character Class" << endl;
	printVector(playerCharClass);
	cin >> playerChoice;
	playerSpawn(playerCharClass, playerChoice);

	//DisplayStats
	playerChoice -= 1;
	if (playerChoice == 0)
	{
		//Assassin
		delete plMage, plWarrior;
		cout << "CLASS: " << playerCharClass.at(playerChoice) << endl
			<< "NAME: " << plAssassin->getName() << endl
			<< "HEALTH: " << plAssassin->getHp() << " / " << plAssassin->getMaxHp() << endl
			<< "POWER: " << plAssassin->getPower() << endl
			<< "VITALITY: " << plAssassin->getVit() << endl
			<< "AGILITY: " << plAssassin->getAgi() << endl
			<< "DEXTERITY: " << plAssassin->getDex() << endl;
	}
	else if (playerChoice == 1)
	{
		//Mage
		delete plAssassin, plWarrior;
		cout << "CLASS: " << playerCharClass.at(playerChoice) << endl
			<< "NAME: " << plMage->getName() << endl
			<< "HEALTH: " << plMage->getHp() << " / " << plMage->getMaxHp() << endl
			<< "POWER: " << plMage->getPower() << endl
			<< "VITALITY: " << plMage->getVit() << endl
			<< "AGILITY: " << plMage->getAgi() << endl
			<< "DEXTERITY: " << plMage->getDex() << endl;
	}
	else if (playerChoice == 2)
	{
		//Warrior
		delete plAssassin, plMage;
		cout << "CLASS: " << playerCharClass.at(playerChoice) << endl
			<< "NAME: " << plWarrior->getName() << endl
			<< "HEALTH: " << plWarrior->getHp() << " / " << plWarrior->getMaxHp() << endl
			<< "POWER: " << plWarrior->getPower() << endl
			<< "VITALITY: " << plWarrior->getVit() << endl
			<< "AGILITY: " << plWarrior->getAgi() << endl
			<< "DEXTERITY: " << plWarrior->getDex() << endl;
	}

	cout << endl << endl << endl;

	//Character Creation (AI)
	Warrior* aiWarrior = new Warrior(aiName);
	Mage* aiMage = new Mage(aiName);
	Assassin* aiAssassin = new Assassin(aiName);
	fillVector(aiCharClass);
	aiChoice;
	randomSpawn(aiCharClass, aiChoice);

	//Display Stats (AI)
	if (aiChoice == 0)
	{
		//Assassin
		delete aiMage, aiWarrior;
		cout << "CLASS: " << playerCharClass.at(aiChoice) << endl
			<< "NAME: " << aiAssassin->getName() << endl
			<< "HEALTH: " << aiAssassin->getHp() << " / " << aiAssassin->getMaxHp() << endl
			<< "POWER: " << aiAssassin->getPower() << endl
			<< "VITALITY: " << aiAssassin->getVit() << endl
			<< "AGILITY: " << aiAssassin->getAgi() << endl
			<< "DEXTERITY: " << aiAssassin->getDex() << endl;

	}
	else if (aiChoice == 1)
	{
		//Mage
		delete aiAssassin, aiWarrior;
		cout << "CLASS: " << playerCharClass.at(aiChoice) << endl
			<< "NAME: " << aiMage->getName() << endl
			<< "HEALTH: " << aiMage->getHp() << " / " << aiMage->getMaxHp() << endl
			<< "POWER: " << aiMage->getPower() << endl
			<< "VITALITY: " << aiMage->getVit() << endl
			<< "AGILITY: " << aiMage->getAgi() << endl
			<< "DEXTERITY: " << aiMage->getDex() << endl;
	}
	else if (aiChoice == 2)
	{
		//Warrior
		delete aiAssassin, aiMage;
		cout << "CLASS: " << playerCharClass.at(aiChoice) << endl
			<< "NAME: " << aiWarrior->getName() << endl
			<< "HEALTH: " << aiWarrior->getHp() << " / " << aiWarrior->getMaxHp() << endl
			<< "POWER: " << aiWarrior->getPower() << endl
			<< "VITALITY: " << aiWarrior->getVit() << endl
			<< "AGILITY: " << aiWarrior->getAgi() << endl
			<< "DEXTERITY: " << aiWarrior->getDex() << endl;
	}
	system("pause");
	system("CLS");

	//BATTLE PHASE
	while (true)
	{
		//MAIN BATTLE DRIVER
		while (true)
		{
			//PLAYER = ASSASSIN
			if (playerChoice == 0)
			{
				//ASSASSIN - ASSASSIN
				if (aiChoice == 0)
				{

					if (plAssassin->getAgi() < aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiAssassin->getName() << " is attacking First!" << endl;
						aiAssassin->attack(plAssassin);
						system("pause");
						if (plAssassin->getHp() <= 0) break;
						cout << endl;
						plAssassin->attack(aiAssassin);
						system("pause");
						system("cls");
						if (aiAssassin->getHp() <= 0) break;
					}
					else if (plAssassin->getAgi() > aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plAssassin->getName() << " is attacking First!" << endl;
						plAssassin->attack(aiAssassin);
						system("pause");
						if (aiAssassin->getHp() <= 0) break;
						cout << endl;
						aiAssassin->attack(plAssassin);
						system("pause");
						system("cls");
						if (plAssassin->getHp() <= 0) break;
					}
					else if (plAssassin->getAgi() == aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plAssassin->getName() << " is attacking First!" << endl;
						plAssassin->attack(aiAssassin);
						system("pause");
						if (aiAssassin->getHp() <= 0) break;
						cout << endl;
						aiAssassin->attack(plAssassin);
						system("pause");
						system("cls");
						if (plAssassin->getHp() <= 0) break;
					}
				}

				//ASSASSIN - MAGE
				else if (aiChoice == 1)
				{
					if (plAssassin->getAgi() < aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiMage->getName() << " is attacking First!" << endl;
						cout << plAssassin->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						aiMage->attack(plAssassin);
						system("pause");
						if (plAssassin->getHp() <= 0) break;
						cout << endl;
						plAssassin->bonusAttack(aiMage);
						system("pause");
						system("cls");
						if (aiMage->getHp() <= 0) break;
					}
					else if (plAssassin->getAgi() > aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plAssassin->getName() << " is attacking First!" << endl;
						cout << plAssassin->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plAssassin->bonusAttack(aiMage);
						system("pause");
						if (aiMage->getHp() <= 0) break;
						cout << endl;
						aiMage->attack(plAssassin);
						system("pause");
						system("cls");
						if (plAssassin->getHp() <= 0) break;
					}
					else if (plAssassin->getAgi() == aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plAssassin->getName() << " is attacking First!" << endl;
						cout << plAssassin->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plAssassin->bonusAttack(aiMage);
						system("pause");
						if (aiMage->getHp() <= 0) break;
						cout << endl;
						aiMage->attack(plAssassin);
						system("pause");
						system("cls");
						if (plAssassin->getHp() <= 0) break;
					}
				}

				//ASSASSIN - WARRIOR
				else if (aiChoice == 2)
				{
					if (plAssassin->getAgi() < aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiWarrior->getName() << " is attacking First!" << endl;
						cout << aiWarrior->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						aiWarrior->bonusAttack(plAssassin);
						system("pause");
						if (plAssassin->getHp() <= 0) break;
						cout << endl;
						plAssassin->attack(aiWarrior);
						system("pause");
						system("cls");
						if (aiWarrior->getHp() <= 0) break;   
					}
					else if (plAssassin->getAgi() > aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plAssassin->getName() << " is attacking First!" << endl;
						cout << aiWarrior->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plAssassin->attack(aiWarrior);
						system("pause");
						if (aiWarrior->getHp() <= 0) break;
						cout << endl;
						aiWarrior->bonusAttack(plAssassin);
						system("pause");
						system("cls");
						if (aiWarrior->getHp() <= 0) break;
					}
					else if (plAssassin->getAgi() == aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plAssassin->getName() << " is attacking First!" << endl;
						cout << aiWarrior->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plAssassin->attack(aiWarrior);
						system("pause");
						if (aiWarrior->getHp() <= 0) break;
						cout << endl;
						aiWarrior->bonusAttack(plAssassin);
						system("pause");
						system("cls");
						if (aiWarrior->getHp() <= 0) break;
					}
				}
			}

			//PLAYER = MAGE
			else if (playerChoice == 1)
			{
				//MAGE - ASSASSIN
				if (aiChoice == 0)
				{
					if (plMage->getAgi() < aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiAssassin->getName() << " is attacking First!" << endl;
						cout << aiAssassin->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						aiAssassin->bonusAttack(plMage);
						system("pause");
						if (plMage->getHp() <= 0) break;
						cout << endl;
						plMage->attack(aiAssassin);
						system("pause");
						system("cls");
						if (aiAssassin->getHp() <= 0) break;
					}
					else if (plMage->getAgi() > aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plMage->getName() << " is attacking First!" << endl;
						cout << aiAssassin->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plMage->attack(aiAssassin);
						system("pause");
						if (aiAssassin->getHp() <= 0) break;
						cout << endl;
						aiAssassin->bonusAttack(plMage);
						system("pause");
						system("cls");
						if (plMage->getHp() <= 0) break;
					}
					else if (plMage->getAgi() == aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plMage->getName() << " is attacking First!" << endl;
						cout << aiAssassin->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plMage->attack(aiAssassin);
						system("pause");
						if (aiAssassin->getHp() <= 0) break;
						cout << endl;
						aiAssassin->bonusAttack(plMage);
						system("pause");
						system("cls");
						if (plMage->getHp() <= 0) break;
					}
				}

				//MAGE - MAGE
				else if (aiChoice == 1)
				{
					if (plMage->getAgi() < aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiMage->getName() << " is attacking First!" << endl;
						aiMage->attack(plMage);
						system("pause");
						if (plMage->getHp() <= 0) break;
						cout << endl;
						plMage->attack(aiMage);
						system("pause");
						system("cls");
						if (aiMage->getHp() <= 0) break;
					}
					else if (plMage->getAgi() > aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plMage->getName() << " is attacking First!" << endl;
						plMage->attack(aiMage);
						system("pause");
						if (aiMage->getHp() <= 0) break;
						cout << endl;
						aiMage->attack(plMage);
						system("pause");
						system("cls");
						if (plMage->getHp() <= 0) break;
					}
					else if (plMage->getAgi() == aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plMage->getName() << " is attacking First!" << endl;
						plMage->attack(aiMage);
						system("pause");
						if (aiMage->getHp() <= 0) break;
						cout << endl;
						aiMage->attack(plMage);
						system("pause");
						system("cls");
						if (plMage->getHp() <= 0) break;
					}
				}

				//MAGE - WARRIOR
				else if (aiChoice == 2)
				{
					if (plMage->getAgi() < aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiWarrior->getName() << " is attacking First!" << endl;
						cout << plMage->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						aiWarrior->attack(plMage);
						system("pause");
						if (plMage->getHp() <= 0) break;
						cout << endl;
						plMage->bonusAttack(aiWarrior);
						system("pause");
						system("cls");
						if (aiWarrior->getHp() <= 0) break;
					}
					else if (plMage->getAgi() > aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plMage->getName() << " is attacking First!" << endl;
						cout << plMage->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plMage->bonusAttack(aiWarrior);
						system("pause");
						if (aiWarrior->getHp() <= 0) break;
						cout << endl;
						aiWarrior->attack(plMage);
						system("pause");
						system("cls");
						if (plMage->getHp() <= 0) break;
					}
					else if (plMage->getAgi() == aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plMage->getName() << " is attacking First!" << endl;
						cout << plMage->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plMage->bonusAttack(aiWarrior);
						system("pause");
						if (aiWarrior->getHp() <= 0) break;
						cout << endl;
						aiWarrior->attack(plMage);
						system("pause");
						system("cls");
						if (plMage->getHp() <= 0) break;
					}
				}
			}

			//PLAYER = WARRIOR
			else if (playerChoice == 2)
			{
				//WARRIOR - ASSASSIN
				if (aiChoice == 0)
				{
					if (plWarrior->getAgi() < aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiAssassin->getName() << " is attacking First!" << endl;
						cout << plWarrior->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						aiAssassin->attack(plWarrior);
						system("pause");
						if (plWarrior->getHp() <= 0) break;
						cout << endl;
						plWarrior->bonusAttack(aiAssassin);
						system("pause");
						system("cls");
						if (aiAssassin->getHp() <= 0) break;
					}
					else if (plWarrior->getAgi() > aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plWarrior->getName() << " is attacking First!" << endl;
						cout << plWarrior->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plWarrior->bonusAttack(aiAssassin);
						system("pause");
						if (aiAssassin->getHp() <= 0) break;
						cout << endl;
						aiAssassin->attack(plWarrior);
						system("pause");
						system("cls");
						if (plWarrior->getHp() <= 0) break;
					}
					else if (plWarrior->getAgi() == aiAssassin->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plWarrior->getName() << " is attacking First!" << endl;
						cout << plWarrior->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plWarrior->bonusAttack(aiAssassin);
						system("pause");
						if (aiAssassin->getHp() <= 0) break;
						cout << endl;
						aiAssassin->attack(plWarrior);
						system("pause");
						system("cls");
						if (plWarrior->getHp() <= 0) break;;
					}
				}

				//WARRIOR - MAGE
				else if (aiChoice == 1)
				{
					if (plWarrior->getAgi() < aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiMage->getName() << " is attacking First!" << endl;
						cout << aiMage->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						aiMage->bonusAttack(plWarrior);
						system("pause");
						if (plWarrior->getHp() <= 0) break;
						cout << endl;
						plWarrior->attack(aiMage);
						system("pause");
						system("cls");
						if (aiMage->getHp() <= 0) break;
					}
					else if (plWarrior->getAgi() > aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plWarrior->getName() << " is attacking First!" << endl;
						cout << aiMage->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plWarrior->attack(aiMage);
						system("pause");
						if (aiMage->getHp() <= 0) break;
						cout << endl;
						aiMage->bonusAttack(plWarrior);
						system("pause");
						system("cls");
						if (plWarrior->getHp() <= 0) break;
					}
					else if (plWarrior->getAgi() == aiMage->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plWarrior->getName() << " is attacking First!" << endl;
						cout << aiMage->getName() << " has been granted [THE BUFF]" << endl
							<< " [THE BUFF]: caster deals 150% more damage." << endl;
						plWarrior->attack(aiMage);
						system("pause");
						if (aiMage->getHp() <= 0) break;
						cout << endl;
						aiMage->bonusAttack(plWarrior);
						system("pause");
						system("cls");
						if (plWarrior->getHp() <= 0) break;
					}
				}

				//WARRIOR - WARRIOR
				else if (aiChoice == 2)
				{
					cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;

					if (plWarrior->getAgi() < aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << aiWarrior->getName() << " is attacking First!" << endl;
						aiWarrior->attack(plWarrior);
						system("pause");
						if (plWarrior->getHp() <= 0) break;
						cout << endl;
						plWarrior->attack(aiWarrior);
						system("pause");
						system("cls");
						if (aiWarrior->getHp() <= 0) break;
					}
					else if (plWarrior->getAgi() < aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plWarrior->getName() << " is attacking First!" << endl;
						plWarrior->attack(aiWarrior);
						system("pause");
						if (aiWarrior->getHp() <= 0) break;
						cout << endl;
						aiWarrior->attack(plWarrior);
						system("pause");
						system("cls");
						if (plWarrior->getHp() <= 0) break;
					}
					else if (plWarrior->getAgi() < aiWarrior->getAgi())
					{
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << playerCharClass.at(playerChoice) << " vs. " << aiCharClass.at(aiChoice) << endl;
						cout << plWarrior->getName() << " is attacking First!" << endl;
						plWarrior->attack(aiWarrior);
						system("pause");
						if (aiWarrior->getHp() <= 0) break;
						cout << endl;
						aiWarrior->attack(plWarrior);
						system("pause");
						system("cls");
						if (plWarrior->getHp() <= 0) break;
					}
				}
			}
		}

		//PLAYER IS ALIVE?
		if ((plWarrior->getHp() <= 0) || (plMage->getHp() <= 0) || (plAssassin->getHp() <= 0)) break;

		//PLAYER WINS -- DELETE AI
		if (playerChoice == 0)
		{
			//ASSASSIN_HEAL and ADD_STATS
			plAssassin->heal();
			plAssassin->addStats();
			cout << "Congratulations! You won. Are you ready for the next battle?" << endl;
			system("pause");
			system("cls");
			cout << "You have been healed!" << endl
				<< "You now have : " << plAssassin->getHp() << " / " << plAssassin->getMaxHp() << endl
				<< "You also got : + 3 AGI ; +3 DEX" << endl;

			cout << "---------------- " << playerCharClass.at(playerChoice) << " ----------------" << endl;
			cout << "CLASS: " << playerCharClass.at(playerChoice) << endl
				<< "NAME: " << plAssassin->getName() << endl
				<< "HEALTH: " << plAssassin->getHp() << " / " << plAssassin->getMaxHp() << endl
				<< "POWER: " << plAssassin->getPower() << endl
				<< "VITALITY: " << plAssassin->getVit() << endl
				<< "AGILITY: " << plAssassin->getAgi() << endl
				<< "DEXTERITY: " << plAssassin->getDex() << endl;

			if (aiChoice == 0) delete aiAssassin, aiCharClass;
			else if (aiChoice == 1) delete aiMage, aiCharClass;
			else if (aiChoice == 2) delete aiWarrior, aiCharClass;

			system("pause");
			system("CLS");

		}
		else if (playerChoice == 1)
		{
			//MAGE_HEAL and ADD_STATS
			plMage->heal();
			plMage->addStats();
			cout << "Congratulations! You won. Are you ready for the next battle?" << endl;
			system("pause");
			system("cls");
			cout << "You have been healed!" << endl
				<< "You now have : " << plMage->getHp() << " / " << plMage->getMaxHp() << endl
				<< "You also got : + 5 POW" << endl;

			cout << "CLASS: " << playerCharClass.at(playerChoice) << endl
				<< "NAME: " << plMage->getName() << endl
				<< "HEALTH: " << plMage->getHp() << " / " << plMage->getMaxHp() << endl
				<< "POWER: " << plMage->getPower() << endl
				<< "VITALITY: " << plMage->getVit() << endl
				<< "AGILITY: " << plMage->getAgi() << endl
				<< "DEXTERITY: " << plMage->getDex() << endl;

			if (aiChoice == 0) delete aiAssassin, aiCharClass;
			else if (aiChoice == 1) delete aiMage, aiCharClass;
			else if (aiChoice == 2) delete aiWarrior, aiCharClass;

			system("pause");
			system("CLS");
		}
		else if (playerChoice == 2)
		{
			//WARRIOR_HEAL and ADD_STATS
			plWarrior->heal();
			plWarrior->addStats();
			cout << "Congratulations! You won. Are you ready for the next battle?" << endl;
			system("pause");
			system("cls");
			cout << "You have been healed!" << endl
				<< "You now have : " << plWarrior->getHp() << " / " << plWarrior->getMaxHp() << endl
				<< "You also got : +3 HP ; +3 VIT" << endl;

			cout << "CLASS: " << playerCharClass.at(playerChoice) << endl
				<< "NAME: " << plWarrior->getName() << endl
				<< "HEALTH: " << plWarrior->getHp() << " / " << plWarrior->getMaxHp() << endl
				<< "POWER: " << plWarrior->getPower() << endl
				<< "VITALITY: " << plWarrior->getVit() << endl
				<< "AGILITY: " << plWarrior->getAgi() << endl
				<< "DEXTERITY: " << plWarrior->getDex() << endl;

			if (aiChoice == 0) delete aiAssassin, aiCharClass;
			else if (aiChoice == 1) delete aiMage, aiCharClass;
			else if (aiChoice == 2) delete aiWarrior, aiCharClass;

			system("pause");
			system("CLS");
		}

		//SPAWN NEW AI (STRONGER)
		//Character Creation (AI)
		Warrior* aiWarrior = new Warrior(aiName);
		Mage* aiMage = new Mage(aiName);
		Assassin* aiAssassin = new Assassin(aiName);
		fillVector(aiCharClass);
		aiChoice;
		randomSpawn(aiCharClass, aiChoice);

		//Display Stats (AI)
		if (aiChoice == 0)
		{
			//Assassin
			delete aiMage, aiWarrior;

			//Get Stronger + 10% STATS
			aiAssassin->isStronger(1.1);

			cout << "CLASS: " << playerCharClass.at(aiChoice) << endl
				<< "NAME: " << aiAssassin->getName() << endl
				<< "HEALTH: " << aiAssassin->getHp() << " / " << aiAssassin->getMaxHp() << endl
				<< "POWER: " << aiAssassin->getPower() << endl
				<< "VITALITY: " << aiAssassin->getVit() << endl
				<< "AGILITY: " << aiAssassin->getAgi() << endl
				<< "DEXTERITY: " << aiAssassin->getDex() << endl;

		}
		else if (aiChoice == 1)
		{
			//Mage
			delete aiAssassin, aiWarrior;

			//Get Stronger + 10% STATS
			aiMage->isStronger(1.1);

			cout << "CLASS: " << playerCharClass.at(aiChoice) << endl
				<< "NAME: " << aiMage->getName() << endl
				<< "HEALTH: " << aiMage->getHp() << " / " << aiMage->getMaxHp() << endl
				<< "POWER: " << aiMage->getPower() << endl
				<< "VITALITY: " << aiMage->getVit() << endl
				<< "AGILITY: " << aiMage->getAgi() << endl
				<< "DEXTERITY: " << aiMage->getDex() << endl;
		}
		else if (aiChoice == 2)
		{
			//Warrior
			delete aiAssassin, aiMage;

			//Get Stronger + 10% STATS
			aiWarrior->isStronger(1.1);

			cout << "CLASS: " << playerCharClass.at(aiChoice) << endl
				<< "NAME: " << aiWarrior->getName() << endl
				<< "HEALTH: " << aiWarrior->getHp() << " / " << aiWarrior->getMaxHp() << endl
				<< "POWER: " << aiWarrior->getPower() << endl
				<< "VITALITY: " << aiWarrior->getVit() << endl
				<< "AGILITY: " << aiWarrior->getAgi() << endl
				<< "DEXTERITY: " << aiWarrior->getDex() << endl;


		}

		system("pause");
		system("CLS");
		gameRound++;

	}

	//END PHASE 
	cout << "Defeat. You have defeated " << gameRound << " enemies." << endl;
	if (playerChoice == 0) delete plAssassin, playerCharClass;
	else if (playerChoice == 1) delete plMage, playerCharClass;
	else if (playerChoice == 2) delete plWarrior, playerCharClass;

	system("pause");
	return 0;
}
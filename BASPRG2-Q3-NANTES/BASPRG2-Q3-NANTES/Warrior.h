#pragma once
#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
using namespace std;

class Mage;
class Assassin;

class Warrior
{
public:
	Warrior(string name);
	~Warrior();

	//Get
	string getName();
	int getHp();
	int getMaxHp();

	int getPower();
	int getVit();
	int getAgi();
	int getDex();

	//Voids
	void takeDamage(int damage);
	void takeBonusDamage(int damage);

	void attack(Warrior* target);
	void bonusAttack(Assassin* target);
	void attack(Mage* target);


	//Heal-On-Win
	void heal();

	//Add Stats `On Win
	int addStats();

	//Ai Get Stronger
	void isStronger(int value);


private:
	string mName;
	int mHp;
	int mMaxHp;
	int mPower;
	int mVit;
	int mAgi;
	int mDex;
	const int bonusDamage = 1.5;
};


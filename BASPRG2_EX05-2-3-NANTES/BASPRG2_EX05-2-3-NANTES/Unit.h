#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
using namespace std;


class Unit
{
public:
	Unit(string name);
	~Unit();

	//Gets
	string getName();
	int getHp();
	int getPow(); 
	int getVit();
	int getAgi();
	int getDex();
	int getHealValue();
	int getStatAddValue();

	//Skills
	void useHeal();
	void useMight();
	void useIronSkin();
	void useConcentration();
	void useHaste();

	
	
protected:
	string mName;
	int mHp, mPow, mVit, mAgi, mDex;
	int HP_HEAL_VAL = 10, STAT_ADD_VAL = 2;


};


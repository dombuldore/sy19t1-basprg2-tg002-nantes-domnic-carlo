#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>
using namespace std;

#include "Unit.h"

void fillVector(vector<string>& buff)
{
	buff.push_back("HEAL");
	buff.push_back("MIGHT");
	buff.push_back("IRON SKIN");
	buff.push_back("CONCENTRATION");
	buff.push_back("HASTE");
}

void printVector(vector<string>& buff)
{
	cout << "What buff do you want to be casted on you?" << endl;

	for (int i = 0; i < buff.size(); i++)
	{
		cout << "[ " << i + 1 << " ] :: " << buff[i] << endl;
	}
}


int main()
{
	srand(time(NULL));
	vector<string> buffName;
	int userChoice;
	char decision;
	string charName;

	fillVector(buffName);

	cout << "Enter your Champion's name : ";
	cin >> charName;
	system("CLS");

	Unit* playerOne = new Unit(charName);

	while (true)
	{
		cout << "------------ PLAYER STATS ------------" << endl;
		cout << "Name : " << playerOne->getName() << endl
			<< "HEALTH : " << playerOne->getHp() << endl
			<< "POWER : " << playerOne->getPow() << endl
			<< "VITALITY : " << playerOne->getVit() << endl
			<< "DEXTERITY : " << playerOne->getDex() << endl
			<< "AGILITY : " << playerOne->getAgi() << endl;

		cout << endl;

		printVector(buffName);
		cout << endl;

		cout << "Input Choice [1 - 5] : ";
		cin >> userChoice;
		cout << endl;
		system("CLS");

		switch (userChoice - 1)
		{
		case 0: 
			playerOne->useHeal();
			break;

		case 1: 
			playerOne->useMight();
			break;

		case 2: 
			playerOne->useIronSkin();
			break;

		case 3: 
			playerOne->useConcentration();
			break;

		case 4:
			playerOne->useHaste();
			break;
		}
	
		cout << "------------ PLAYER STATS ------------" << endl;
		cout << "Name : " << playerOne->getName() << endl
			<< "HEALTH : " << playerOne->getHp() << endl
			<< "POWER : " << playerOne->getPow() << endl
			<< "VITALITY : " << playerOne->getVit() << endl
			<< "DEXTERITY : " << playerOne->getDex() << endl
			<< "AGILITY : " << playerOne->getAgi() << endl;
		
		cout << "Do you want to Continue? [Y/N] : ";
		cin >> decision;
		if (decision != 'y') break;
		system("CLS");
	}

	cout << endl << endl << endl << endl << "Thank you for playing!" << endl << endl << endl << endl;
	
	system("pause");
	return 0;
}
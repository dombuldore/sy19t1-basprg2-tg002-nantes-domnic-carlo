#include "Unit.h"

Unit::Unit(string name)
{
	mName = name;
	mHp = 15;
	mPow = 14;
	mVit = 13;
	mAgi = 12;
	mDex = 10;
}

Unit::~Unit()
{
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getDex()
{
	return mDex;
}

int Unit::getHealValue()
{
	return HP_HEAL_VAL;
}

int Unit::getStatAddValue()
{
	return STAT_ADD_VAL;
}

void Unit::useHeal()
{
	cout << "You have casted [HEAL]!" << endl
		<< "[HEAL] : Permanently gain +10 Health Points." << endl;
	mHp += HP_HEAL_VAL;
}

void Unit::useMight()
{
	cout << "You have casted [MIGHT]!" << endl
		<< "[MIGHT] : Permanently gain +2 Power Points." << endl;
	mPow += 2;
}

void Unit::useIronSkin()
{
	cout << "You have casted [IRON SKIN]!" << endl
		<< "[IRON SKIN] : Permanently gain +2 Vitality Points." << endl;
	mVit += 2;
}

void Unit::useConcentration()
{
	cout << "You have casted [CONCENTRATION]!" << endl
		<< "[CONCENTRATION] : Permanently gain +2 Agility Points." << endl;
	mAgi += 2;
}

void Unit::useHaste()
{
	cout << "You have casted [HASTE]!" << endl
		<< "[HASTE] : Permanently gain +2 Dexterity Points." << endl;
}

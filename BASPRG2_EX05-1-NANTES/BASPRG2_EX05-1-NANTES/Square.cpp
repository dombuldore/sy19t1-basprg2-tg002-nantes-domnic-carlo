#include "Square.h"


Square::Square() : Shape("Square", 4)
{
}

Square::Square(string name, int sides) : Shape(name, sides)
{
}


Square::~Square()
{
}

float Square::getLength()
{
	return mLength;
}

void Square::setLength(float l)
{
	mLength = l;
}

float Square::getArea()
{
	return pow(mLength, 2);
}

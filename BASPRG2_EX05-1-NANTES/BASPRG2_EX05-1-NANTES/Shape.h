#pragma once
#include <iostream>
#include <string>

using namespace std;

class Shape
{
public:
	Shape(string name, int sides);
	~Shape();

	string getName();
	virtual float getArea() = 0;

private:
	string mName;
	int mNumSides;
};


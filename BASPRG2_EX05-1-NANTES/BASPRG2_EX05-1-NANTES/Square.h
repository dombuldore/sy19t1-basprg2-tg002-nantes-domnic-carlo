#pragma once
#include "Shape.h"

using namespace std;

class Square : public Shape

{
public:
	Square();
	Square(string name, int sides);
	~Square();

	float getLength();
	void setLength(float l);
	float getArea() override;

private:
	float mLength;

};


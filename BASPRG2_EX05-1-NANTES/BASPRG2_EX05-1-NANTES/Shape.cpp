#include "Shape.h"

Shape::Shape(string name, int sides)
{
	mName = name;
	mNumSides = sides;
}

Shape::~Shape()
{
}

string Shape::getName()
{
	return mName;
}

float Shape::getArea()
{
	return 0.0f;
}

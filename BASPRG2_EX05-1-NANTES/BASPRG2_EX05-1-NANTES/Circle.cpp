#include "Circle.h"


Circle::Circle() : Shape("Circle", 0)
{
}

Circle::~Circle()
{
}

float Circle::getRadius()
{
	return mRadius;
}

void Circle::setRadius(float r)
{
	mRadius = r;
}

float Circle::getArea()
{
	return 3.14 * pow(mRadius, 2);
}

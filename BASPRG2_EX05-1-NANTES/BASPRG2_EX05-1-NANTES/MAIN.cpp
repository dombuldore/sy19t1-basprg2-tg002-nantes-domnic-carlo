#include <iostream>
#include <conio.h>
#include <vector>
#include <string>
#include "Square.h"
#include "Circle.h"

using namespace std;



void printShapes(const vector<Shape*>& shapes)
{
	for (int x = 0; x < shapes.size(); x++)
	{
		Shape* shape = shapes[x];
		cout << shape->getName() << " Area: " << shape->getArea() << endl;
	}

}

int main()
{
	vector<Shape*> shapes;

	Square* square = new Square();
	square->setLength(5.0f);
	shapes.push_back(square);

	Circle* circle = new Circle();
	circle->setRadius(10.0f);
	shapes.push_back(circle);

	printShapes(shapes);
	system("pause");
}


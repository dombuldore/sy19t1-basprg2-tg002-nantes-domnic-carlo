#pragma once
#include <iostream>
#include <string>
using namespace std;

class weapon;

class character
{
public:
	character();
	~character();

	int getStr();
	int getDex();
	int getVit();
	int getMagic();
	int getSpirit();
	int getLuck();
	string getName();
	int getHp();
	int getMp();
	int getExp();

	void setStr(int value);
	void setDex(int value);
	void setVit(int value);
	void setMagic(int value);
	void setSpirit(int value);
	void setLuck(int value);
	void setName(string value);
	void setHp(int value);
	void setMp(int value);
	void setExp(int value);

	void takeDamage(int damage);

	int getArmor();
	void setArmor(int value);
	
	//equipping weapon
	weapon* getWeapon();
	void equipWeapon(weapon* weapon);

	//attack
	void attack(character* target);

private:
	int mStr;
	int mDex;
	int mVit;
	int mMagic;
	int mSpirit;
	int mLuck;

	string mName;
	int mHp;
	int mMp;
	int mExp;

	int mArmor;

	weapon* mWeapon;

};


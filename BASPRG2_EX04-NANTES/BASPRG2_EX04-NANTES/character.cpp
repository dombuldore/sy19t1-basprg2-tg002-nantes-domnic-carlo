#include "character.h"
#include "weapon.h"

character::character()
{

}

character::~character()
{

}

int character::getStr()
{
	return mStr;
}

int character::getDex()
{
	return mDex;
}

int character::getVit()
{
	return mVit;
}

int character::getMagic()
{
	return mMagic;
}

int character::getSpirit()
{
	return mSpirit;
}

int character::getLuck()
{
	return mLuck;
}

string character::getName()
{
	return mName;
}

int character::getHp()
{
	return mHp;
}

int character::getMp()
{
	return mMp;
}

int character::getExp()
{
	return mExp;
}

void character::setStr(int value)
{
	value = mStr;
}

void character::setDex(int value)
{
	value = mDex;
}

void character::setVit(int value)
{
	value = mVit;
}

void character::setMagic(int value)
{
	value = mMagic;
}

void character::setSpirit(int value)
{
	value = mSpirit;
}

void character::setLuck(int value)
{
	value = mLuck;
}

void character::setName(string value)
{
	value = mName;
}

void character::setHp(int value)
{
	value = mHp;
	if (mHp < 0) mHp = 0;
}

void character::setMp(int value)
{
	value = mMp;
}

void character::setExp(int value)
{
	value = mExp;
}

void character::takeDamage(int damage)
{
	if (damage < 0) return;
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

int character::getArmor()
{
	return mArmor;
}

void character::setArmor(int value)
{
	value = mArmor;
}

weapon* character::getWeapon()
{
	return mWeapon;
}

void character::equipWeapon(weapon* weapon)
{
	if (weapon != nullptr) delete mWeapon;
	mWeapon = weapon;
}

void character::attack(character* target)
{
}

void character::attack(character* target)
{
	int damage = mStr - target->getArmor();
	if (damage < 1) damage = 1;

	target->takeDamage(damage);
}

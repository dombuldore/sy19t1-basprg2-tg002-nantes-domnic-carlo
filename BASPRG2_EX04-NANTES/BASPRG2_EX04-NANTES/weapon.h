#pragma once
#include <iostream>
#include <string>
using namespace std;


class weapon
{
public:
	weapon();
	~weapon();

	string getName();
	void setName(string weaponName);

	int getPower();
	void setPower(int value);

private:
	string mName;
	int mPow;
};


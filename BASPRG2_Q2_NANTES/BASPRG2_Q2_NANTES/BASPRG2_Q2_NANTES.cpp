#include <iostream>
#include <time.h>
#include <string>

using namespace std;

//NODE.H BLOCK
#ifndef NODE_H
#define NODE_H

struct Node
{
	std::string name;
	Node* next = NULL;
	Node* previous = NULL;
};
#endif //NODE_H

//FUNCTIONS BLOCK (They're Below)
Node* nodeCount(int memberCount);
void displayCrows(Node* crows, int memberCount);
Node* createCrow(Node* crows, string crowName);
Node* lordCommander(Node* startingCrow, int startingPosition);
Node* killMember(Node* unfortunateCrow, int unfortunatePosition);
void deadCrow(Node* deceasedCrow, int killDice);

//MAIN
int main()
{
	srand(time(NULL));
	string memberName;
	char decision;
	int memberCount;
	
	Node* nightsWatch = new Node;
	
	//Asking for Number of Members (DYNAMIC)
	cout << "How many members do you want to fight for THE WALL?" << endl << "NUMBERS 1-10 ONLY :: ";
	cin >> memberCount;
	nightsWatch = nodeCount(memberCount);
	system("CLS");

	//Entering member Names
	for (int j = 1; j <= memberCount; j++)
	{
		cout << "Enter the name of your Night's Watch member [ " << j << " ] ::: ";
		cin >> memberName;
		cout << endl;
		nightsWatch = createCrow(nightsWatch, memberName);
	}

	system("CLS");

	//Lists the Members, Lord Commander's Chosen One, Dice Roll 
	int chosenOne;
	displayCrows(nightsWatch, memberCount);
	chosenOne = rand() % memberCount + 1;
	nightsWatch = lordCommander(nightsWatch, chosenOne);
	cout << "Type in any letter to continue. :: ";
	cin >> decision;
	system("CLS");

	//Pre-Game STATS
	cout << "LORD COMMANDER has CHOSEN   " << nightsWatch->name << "   to START the game." << endl << endl;
	cout << "New Arrangement for the Night's Watch members." << endl;
	displayCrows(nightsWatch, memberCount);

	cout << endl << endl;

	cout << "Type in any letter to continue. :: ";
	cin >> decision;
	system("CLS");

	//Gameplay Proper
	int stageNumber = 1;
	while (memberCount != 1)
	{
		int killDice = rand() % memberCount + 1;

		cout << " >>>>>>>>>> ROUND " << stageNumber << " <<<<<<<<<<" << endl;
		cout << "DICE ROLL = " << killDice << endl; 

		displayCrows(nightsWatch, memberCount);
		deadCrow(nightsWatch, killDice);
		nightsWatch = killMember(nightsWatch, killDice);
		
		stageNumber++;
		memberCount--;
		cout << "Type in any letter to continue.  :: ";
		cin >> decision;
		system("CLS");
	}

	//End Game Flavor Text
	cout << endl << endl << endl;
	cout << "All players EXCEPT FOR " << nightsWatch->name << " has been ELIMINATED." << endl;
	cout << "The New Gods whispered the name   " << nightsWatch->name << "   to call for Reinforcements." << endl;
	cout << endl << endl;
	cout << " >>>>>>>>>> " << nightsWatch->name << "  has Won the match. <<<<<<<<<< " << endl;
	cout << "THE END." << endl;

	system("pause");
	return 0;
}

//Creating nodes for number of members. (FUNCTION)
Node* nodeCount(int memberCount)
{
	Node* nightsWatchHead = new Node;
	Node* nightsWatchTail = nightsWatchHead;
	for (int i = 1; i < memberCount; i++)
	{
		Node* newNightsWatchMember = new Node;
		nightsWatchTail->next = newNightsWatchMember;
		nightsWatchTail = nightsWatchTail->next;
	}

	nightsWatchTail->next = nightsWatchHead;
	return nightsWatchHead;
}

//Creating a Crow. (FUNCTIONS)
Node* createCrow(Node* crows, string crowName)
{
	crows->name = crowName;
	crows = crows->next;
	return crows;
}


//Printing the Names of the Nodes. (FUNCTION)
void displayCrows(Node* crows, int memberCount)
{
	cout << "Here are your REMAINING Watchers on the Wall." << endl;
	for (int k = 1; k <= memberCount; k++)
	{

		cout << "Defender of the Wall # " << k << " : " << crows->name << endl;
		crows = crows->next;
	}
}

//Lord Commander choice. (FUNCTION)
Node* lordCommander(Node* startingCrow, int startingPosition)
{
	for (int a = 0; a < startingPosition; a++)
	{
		startingCrow = startingCrow->next;
	}

	return startingCrow;
}

//Eliminating member. (FUNCTION)
Node* killMember(Node* unfortunateCrow, int unfortunatePosition)
{
	Node* strongOne = new Node;
	for (int b = 0; b < unfortunatePosition; b++)
	{
		strongOne = unfortunateCrow;
		unfortunateCrow = unfortunateCrow->next;
	}

	strongOne->next = unfortunateCrow->next;
	delete unfortunateCrow;
	strongOne = strongOne->next;
	return strongOne;
}

//Announcing who got eliminated. (FUNCTION)
void deadCrow(Node* deceasedCrow, int killDice)
{
	Node* tempCrow = deceasedCrow;
	for (int c = 0; c < killDice; c++)
	{
		tempCrow = tempCrow->next;
	}

	cout << endl << endl << "The Old Gods whispered the name   " << tempCrow->name << "   and has been ELIMINATED. " << endl;
}
#include "Spell.h"

Spell::Spell()
{
	mName = "Energy Ball";
	mPower = 1;
	mCost = 1;
}

Spell::Spell(string spellName, int spellPower, int manaCost)
{
	mName = spellName;
	mPower = spellPower;
	mCost = manaCost;

}

Spell::~Spell()
{
}

string Spell::getName()
{
	return mName;
}

int Spell::getPower()
{
	return mPower;
}

int Spell::getCost()
{
	return mCost;
}

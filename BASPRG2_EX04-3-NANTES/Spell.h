#pragma once
#include <iostream>
#include <string>
using namespace std;


class Spell
{
public:

	Spell();
	Spell(string spellName, int spellPower, int manaCost);
	~Spell();

	string getName();
	int getPower();
	int getCost();

private:
	string mName;
	int mPower;
	int mCost;
};


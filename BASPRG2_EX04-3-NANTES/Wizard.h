#pragma once
#include <iostream>
#include <string>
using namespace std;

class Spell;

class Wizard
{
public:
	Wizard();
	Wizard(string name, int health, int mana);
	~Wizard();

	string getName();
	int getHp();
	int getMp();

	int getArmor();
	void setArmor(int value);

	Spell* getSpell();
	Spell* setSpell(Spell* spell);

	void takeDamage(int damage);
	void attack(Wizard* target);


private:
	string mName;
	int mArmor;
	int mHp;
	int mMp;
	Spell* mSpell;
};


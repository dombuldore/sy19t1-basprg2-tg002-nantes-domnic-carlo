#include "Wizard.h"
#include "Spell.h"

Wizard::Wizard()
{
	mName = "Randalf the Gray";
	mHp = 1;
	mMp = 1;
	mArmor = 1;
}

Wizard::Wizard(string name, int health, int mana)
{
	mName = name;
	mHp = health;
	mMp = mana;
}

Wizard::~Wizard()
{
}

string Wizard::getName()
{
	return mName;
}

int Wizard::getHp()
{
	return mHp;
}

int Wizard::getMp()
{
	return mMp;
}

int Wizard::getArmor()
{
	return mArmor;
}

void Wizard::setArmor(int value)
{
	mArmor = value;
}

Spell* Wizard::getSpell()
{
	return mSpell;
}

Spell* Wizard::setSpell(Spell* spell)
{
	if (spell != nullptr) delete mSpell;
	mSpell = spell;
	return mSpell;
}

void Wizard::takeDamage(int damage)
{
	if (damage < 0) return;
	mHp -= damage;
	if (mHp < 0) mHp = 0;
}

void Wizard::attack(Wizard* target)
{
	int damage = mSpell->getPower() - target->getArmor();
	if (damage < 0) damage = 1;
	target->takeDamage(damage);
	cout << target->getName() << " 's remaining health :: " << target->getHp() << endl;
}

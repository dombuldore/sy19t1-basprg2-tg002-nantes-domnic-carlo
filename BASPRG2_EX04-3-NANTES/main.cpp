#include <iostream>
#include <string>

using namespace std;

#include "Wizard.h"
#include "Spell.h"

int main()
{
	char decision;

	Wizard* wizard1 = new Wizard("Gandalf the Grey", 32000, 100000);
	Spell* fireball1 = new Spell("Volcanic Ball", 3250, 1000);
	wizard1->setSpell(fireball1);
	wizard1->setArmor(50);
	cout << "Name : " << wizard1->getName() << endl
		<< "Health : " << wizard1->getHp() << endl
		<< "Mana : " << wizard1->getMp() << endl;

	cout << endl << endl;

	Wizard* wizard2 = new Wizard("Saruman", 26500, 82500);
	Spell* fireball2 = new Spell("Fireball", 2700, 825);
	wizard2->setSpell(fireball2);
	wizard2->setArmor(50);
	cout << "Name : " << wizard2->getName() << endl
		<< "Health : " << wizard2->getHp() << endl
		<< "Mana : " << wizard2->getMp() << endl;

	cout << endl << endl;

	while (true)
	{
		cout << wizard1->getName() << " casts " << fireball1->getName() << endl;
		cout << wizard1->getName() << " has dealt " << fireball1->getPower() << " damage to " << wizard2->getName() << endl;
		wizard1->attack(wizard2);

		cout << endl;

		cout << wizard2->getName() << " casts " << fireball2->getName() << endl;
		cout << wizard2->getName() << " has dealt " << fireball2->getPower() << " damage to " << wizard1->getName() << endl;
		wizard2->attack(wizard1);

		if (wizard1->getHp() <= 0 || wizard2->getHp() <= 0) break;

		system("pause");
		system("cls");
	}

	system("cls");
	//Declaring winner; deleting everything
	if (wizard1->getHp() != 0)
	{
		cout << wizard1->getName() << " WINS! Thank you for playing." << endl;
		delete wizard1, wizard2, fireball1, fireball2;
	}

	else
	{
		cout << wizard2->getName() << " WINS! Thank you for playing." << endl;
		delete wizard1, wizard2, fireball1, fireball2;
	}


	system("pause");
	return 0;
}